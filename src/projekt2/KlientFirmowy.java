/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;


public class KlientFirmowy extends Klient {

    private String adresKorespondencyjny;
    private int[] numerKonta = new int[26];
    private int[] regon = new int[14];
    
    public KlientFirmowy(Dane d, int nr){
        super(d,nr);
        d.setLokalizacja((this.wspolrzedne.x-10)/80, (this.wspolrzedne.y-10)/80, 3);

        this.adresKorespondencyjny = randomAlphabetic(25);
        
        for(int i=0; i<26; i++)
            this.numerKonta[i] = ThreadLocalRandom.current().nextInt(10);
        
        for (int i=0; i<14; i++)
            this.regon[i] = ThreadLocalRandom.current().nextInt(10);
    }
    
    @Override
    public String toString() {
        return "Firma " + super.toString() + ", adres korespondencyjny: " 
               + getAdresKorespondencyjny() + ", konto: " + Arrays.toString(getNumerKonta())
               + ", regon: " + Arrays.toString(getRegon());
    }
    
    /**
     * @return adres korespondencyjny firmy
     */
    public String getAdresKorespondencyjny() {
        return adresKorespondencyjny;
    }

    /**
     * @return numer konta firmy
     */
    public int[] getNumerKonta() {
        return numerKonta;
    }

    /**
     * @return numer regon firmy
     */
    public int[] getRegon() {
        return regon;
    }
}
