/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

/**
 *
 * @author Inga Spychała
 */
public class Samochod extends Pojazd{
    public Samochod(){
        super();
        this.ladownosc = 3;
        this.predkosc = 50;
        this.pojemnoscBaku = 500;
        this.stanPaliwa = this.pojemnoscBaku;
    }
    
    @Override
    public String toString() { 
        return "Samochód; " + super.toString();
    }
}
