/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.io.IOException;

/**
 * Główna klasa programu, tworząca nową restaurację
 * @author Inga Spychała
 */
public class MainClass {

    public static void main(String[] args) throws IOException, InterruptedException {
        Restauracja res = new Restauracja();
        while (true) {
            res.repaint();
            Thread.sleep(50);
        }
    }
}
