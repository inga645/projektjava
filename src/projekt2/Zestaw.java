/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author Inga Spychała
 */
public class Zestaw extends Posilek{
    private Posilek[] skladZestawu;
   // private String nazwaZestawu;
    private double znizka;
    
    /**
     *
     * @param m menu restauracji
     */
    public Zestaw(Menu m){
        super(m);
        this.znizka = 0.1;
        this.cena = 0;
        this.czasPrzygotowania = 0;
        int x = ThreadLocalRandom.current().nextInt(2,6);
        this.skladZestawu = new Posilek[x];
        for(int i = 0; i < x; i++){
            int y = ThreadLocalRandom.current().nextInt(m.getListaPosilkow().size()); 
            skladZestawu[i] = m.getListaPosilkow().get(y); 
            this.cena += m.getListaPosilkow().get(y).getCena();
            this.czasPrzygotowania += m.getListaPosilkow().get(y).getCzasPrzygotowania();
        }
    }
    
    /**
     * Oblicza cenę zestawu uwzględniając ceny wszystkich posiłków składowych oraz procent zniżki
     */
    public void obliczCene(){
        this.cena *= (1-this.znizka);
    }
    
    @Override
    public String toString(){
        return "zestaw: " + this.nazwa + ", skład: " + Arrays.toString(skladZestawu);
    }

}
