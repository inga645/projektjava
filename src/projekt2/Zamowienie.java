/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.awt.Point;
import java.io.Serializable;
import static java.lang.Math.abs;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Klasa opisująca zamówienia klientów
 *
 * @author Inga Spychała
 */
public class Zamowienie implements Serializable{

    private Posilek[] listaPosilkow;
    private double cenaPosilkow = 0;
    private double odleglosc;
    private double znizka = 0;
    private int czasPrzygotowania = 0;
    private Dane info;
    private int idKlienta;
    private Point lokalizacja;

    /**
     * Tworzenie losowego zamówienia
     *
     * @param d dane restauracji
     */
    public Zamowienie(Dane d) {
        this.info = d;

        int z = ThreadLocalRandom.current().nextInt(1, 6);
        this.listaPosilkow = new Posilek[z];
        int lzestawow = ThreadLocalRandom.current().nextInt(z);
        int lposilkow = z - lzestawow;

        for (int i = 0; i < lzestawow; i++) {
            int tmpnr = d.getM().getListaZestawow().size();
            int tmp = ThreadLocalRandom.current().nextInt(tmpnr);
            this.listaPosilkow[i] = d.getM().getListaZestawow().get(tmp);
            this.czasPrzygotowania += d.getM().getListaZestawow().get(tmp).getCzasPrzygotowania();
            this.cenaPosilkow += d.getM().getListaZestawow().get(tmp).getCena();
        }
        for (int i = lzestawow; i < z; i++) {
            int tmpnr = d.getM().getListaPosilkow().size();
            int tmp = ThreadLocalRandom.current().nextInt(tmpnr);
            this.listaPosilkow[i] = d.getM().getListaPosilkow().get(tmp);
            this.czasPrzygotowania += d.getM().getListaPosilkow().get(tmp).getCzasPrzygotowania();
            this.cenaPosilkow += d.getM().getListaPosilkow().get(tmp).getCena();
        }
        obliczCene();

        int x = ThreadLocalRandom.current().nextInt(3);
        int y;
        int a;
        switch (x) {
            case 0:
                y = ThreadLocalRandom.current().nextInt(d.getListaKlientowOkazjonalnych().size());
                a = d.getListaKlientowOkazjonalnych().get(y).getNum();
                this.lokalizacja = d.getListaKlientowOkazjonalnych().get(y).getWspolrzedne();
                //d.getListaKlientowOkazjonalnych().get(y).setKwotaZamowienia(cenaPosilkow);
                break;
            case 1:
                y = ThreadLocalRandom.current().nextInt(d.getListaKlientowStalych().size());
                a = d.getListaKlientowStalych().get(y).getNum();
                if (d.getListaKlientowStalych().get(y).isZnizka()) {
                    this.cenaPosilkow -= 100;
                    if(cenaPosilkow<0){
                        this.cenaPosilkow = 0;
                    }
                    d.getListaKlientowStalych().get(y).setZnizka(false);
                }
                this.lokalizacja = d.getListaKlientowStalych().get(y).getWspolrzedne();
                d.getListaKlientowStalych().get(y).setIloscPunktow(lzestawow*5+lposilkow*2);
                //d.getListaKlientowStalych().get(y).setKwotaZamowienia(cenaPosilkow);
                break;
            default:
                y = ThreadLocalRandom.current().nextInt(d.getListaKlientowFirmowych().size());
                a = d.getListaKlientowFirmowych().get(y).getNum();
                this.lokalizacja = d.getListaKlientowFirmowych().get(y).getWspolrzedne();
               // d.getListaKlientowFirmowych().get(y).setKwotaZamowienia(cenaPosilkow);
                break;
        }
        this.odleglosc = abs(330 - this.lokalizacja.x) / 80 + abs(180 - this.lokalizacja.y) / 80;
        this.idKlienta = a;

    }

    /**
     * Tworzenie zamówienia przez konkretnego klienta
     *
     * @param d dane restauracji
     * @param nr numer ID klienta
     * @param p lokalizacja klienta na mapie
     * @param znizka zniżka danego klienta
     */
    public Zamowienie(Dane d, int nr, Point p, double znizka) {
        this.info = d;

        int z = ThreadLocalRandom.current().nextInt(1, 6);
        this.listaPosilkow = new Posilek[z];
        int lzestawow = ThreadLocalRandom.current().nextInt(z);
        int lposilkow = z - lzestawow;

        for (int i = 0; i < lzestawow; i++) {
            int tmpnr = d.getM().getListaZestawow().size();
            int tmp = ThreadLocalRandom.current().nextInt(tmpnr);
            this.listaPosilkow[i] = d.getM().getListaZestawow().get(tmp);
            this.czasPrzygotowania += d.getM().getListaZestawow().get(tmp).getCzasPrzygotowania();
            this.cenaPosilkow += d.getM().getListaZestawow().get(tmp).getCena();
        }
        for (int i = lzestawow; i < z; i++) {
            int tmpnr = d.getM().getListaPosilkow().size();
            int tmp = ThreadLocalRandom.current().nextInt(tmpnr);
            this.listaPosilkow[i] = d.getM().getListaPosilkow().get(tmp);
            this.czasPrzygotowania += d.getM().getListaPosilkow().get(tmp).getCzasPrzygotowania();
            this.cenaPosilkow += d.getM().getListaPosilkow().get(tmp).getCena();
        }
        this.lokalizacja = p;
        this.odleglosc = abs(330 - p.x) / 80 + abs(180 - p.y) / 80;
        this.idKlienta = nr;
        this.znizka = znizka;
        obliczCene();
        
        if(nr > 1000 && nr < 2000){
            d.getListaKlientowStalych().stream().filter((klient) -> (klient.getNum() == nr)).forEachOrdered((klient) -> {
                klient.setIloscPunktow(lzestawow*3+lposilkow*1);
                klient.setKwotaZamowienia(cenaPosilkow);
            });
        }
    }

    /**
     * Tworzy zamówienie w czasie określonym dla posiłków składowych
     */
    public void przygotujZamowienie() {
        try {
            Thread.sleep(this.czasPrzygotowania*500);
        } catch (InterruptedException ex) {
            Logger.getLogger(Zamowienie.class.getName()).log(Level.SEVERE, null, ex);
        }
        info.modifyListaZamowien(this, -1);
        info.modifyGotoweZamowienia(this, 1);
        System.out.println("zamowienie gotowe");
    }

    /**
     * Oblicza cenę zamówienia, na którą składają się ceny poszczególnych
     * posiłków oraz koszty dostawy
     */
    public void obliczCene() {
        this.cenaPosilkow = this.getCenaPosilkow() * (1 - this.znizka / 100) + this.odleglosc;
    }

    @Override
    public String toString() {
        return "Zamówienie: czas " + czasPrzygotowania + ", cena: " + cenaPosilkow;
    }

    /**
     * @return znizka
     */
    public double getZnizka() {
        return znizka;
    }

    /**
     * @param znizka znizka
     */
    public void setZnizka(double znizka) {
        this.znizka = znizka;
    }

    /**
     * @return cena posiłków
     */
    public double getCenaPosilkow() {
        return cenaPosilkow;
    }

    /**
     * @param odleglosc odległość klienta od restauracji
     */
    public void setOdleglosc(double odleglosc) {
        this.odleglosc = odleglosc;
    }

    /**
     * @return lokalizacja klienta
     */
    public Point getLokalizacja() {
        return lokalizacja;
    }
}
