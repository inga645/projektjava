/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.awt.Point;
import java.time.Instant;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.ThreadLocalRandom;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Klasa opisująca klientów restauracji
 *
 * @author Inga Spychała
 */
public class Klient extends Osoba {

    private String nazwa;
    int id;
    private int[] nrTelefonu = new int[9];
    private String adresDostawy;
    private String[] zamowienie;
    private double kwotaZamowienia;
    Instant czasZamowienia;
    private String email;
    int znizkaProcentowa = 0;
    Dane info;
    boolean zamowienieOdebrane;

    public Klient(Dane d, int nr) {
        this.nazwa = randomAlphabetic(15);
        this.adresDostawy = randomAlphabetic(25);
        this.email = randomAlphabetic(5, 10) + "@mail.com";
        for (int i = 0; i < 9; i++) {
            this.nrTelefonu[i] = ThreadLocalRandom.current().nextInt(10);
        }
        this.info = d;
        this.id = nr;

        int x = 0;
        int y = 0;
        do {
            x = ThreadLocalRandom.current().nextInt(8);
            y = ThreadLocalRandom.current().nextInt(5);
        } while ((info.getLokalizacja(x, y)) != 0);
        this.wspolrzedne = new Point(10 + 80 * x, 20 + 80 * y);

    }

    /**
     * @return adresDostawy
     */
    public String getAdresDostawy() {
        return adresDostawy;
    }

    /**
     * @return zamowienie
     */
    public String[] getZamowienie() {
        return zamowienie;
    }

    /**
     * @return czasZamowienia
     */
    public Instant getCzasZamowienia() {
        return czasZamowienia;
    }

    /**
     * Klient cyklicznie składa zamówienia
     */
    @Override
    public void run() {
        while (true) {
            zlozZamowienie();
            zamowienieOdebrane = false;
            System.out.println("zamowienie" + this.id);
            this.czasZamowienia = Instant.now();
            int wait = ThreadLocalRandom.current().nextInt(1, 15);
            try {
                Thread.sleep(1000 * wait);
            } catch (InterruptedException ex) {
                Logger.getLogger(Klient.class.getName()).log(Level.SEVERE, null, ex);
            }
            while (!zamowienieOdebrane) {
                try {
                    Thread.sleep(1000 * 1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Klient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Tworzy nowe zamówienie
     */
    public void zlozZamowienie() {
        Zamowienie z = new Zamowienie(info, id, wspolrzedne, znizkaProcentowa);
        this.setKwotaZamowienia(z.getCenaPosilkow());
        info.modifyListaZamowien(z, 1);
        z.przygotujZamowienie();
    }

    @Override
    public String toString() {
        return "Klient: " + id + " " + getNazwa() + ", tel. " + Arrays.toString(getNrTelefonu())
                + ", adres: " + adresDostawy + ", email: " + getEmail()
                + ", kwota: " + getKwotaZamowienia() + ", czas: " + czasZamowienia;
    }

    /**
     * @return numer ID klienta
     */
    public int getNum() {
        return id;
    }

    /**
     * @return numer telefonu klienta
     */
    public int[] getNrTelefonu() {
        return nrTelefonu;
    }

    /**
     * @return kwota zamowienia
     */
    public double getKwotaZamowienia() {
        return kwotaZamowienia;
    }

    /**
     * @return adres email klienta
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return nazwa klienta
     */
    public String getNazwa() {
        return nazwa;
    }

    /**
     * @param kwotaZamowienia kwota aktualnego zamowienia
     */
    public void setKwotaZamowienia(double kwotaZamowienia) {
        this.kwotaZamowienia = kwotaZamowienia;
    }

}
