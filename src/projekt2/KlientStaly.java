/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.time.Instant;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KlientStaly extends Klient {

    private int progPunktowy;
    private int iloscPunktow;
    private boolean znizka = false;

    public KlientStaly(Dane d, int nr) {
        super(d, nr);
        this.znizkaProcentowa = 5;
        d.setLokalizacja((this.wspolrzedne.x - 10) / 80, (this.wspolrzedne.y - 10) / 80, 2);
    }

    /**
     * Klient stały cyklicznie składa zamówienia i otrzymuje punkty, za które
     * może otrzymać zniżkę
     */
    @Override
    public void run() {
        while (true) {
            zlozZamowienie();
            zamowienieOdebrane = false;
            System.out.println("zamowienie" + this.id);
            this.czasZamowienia = Instant.now();
            znizkaZaPunkty();
            int wait = ThreadLocalRandom.current().nextInt(1, 15);
            try {
                Thread.sleep(1000 * wait);
            } catch (InterruptedException ex) {
                Logger.getLogger(Klient.class.getName()).log(Level.SEVERE, null, ex);
            }

            while (!zamowienieOdebrane) {
                try {
                    Thread.sleep(1000 * 1);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Klient.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    /**
     * Jeśli klient przekroczył próg punktowy, cena jego kolejnego zamówienia
     * obniżana jest o 100zł
     */
    public void znizkaZaPunkty() {
        if (getIloscPunktow() >= progPunktowy) {
            setZnizka(true);
            setIloscPunktow(getIloscPunktow() - progPunktowy);
        }
    }

    @Override
    public String toString() {
        return "Stały " + super.toString() + ", punkty: " + getIloscPunktow();
    }

    /**
     * @param iloscPunktow liczba punktów do dodania
     */
    public void setIloscPunktow(int iloscPunktow) {
        this.iloscPunktow += iloscPunktow;
    }

    /**
     * @return czy można zastosować zniżkę
     */
    public boolean isZnizka() {
        return znizka;
    }

    /**
     * @param znizka czy przy następnym zamówieniu klient otrzyma zniżkę
     */
    public void setZnizka(boolean znizka) {
        this.znizka = znizka;
    }

    /**
     * @return liczba punktów
     */
    public int getIloscPunktow() {
        return iloscPunktow;
    }
}
