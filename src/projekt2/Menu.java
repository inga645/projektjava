/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;
import java.io.Serializable;
import java.util.ArrayList;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Klasa opisująca menu restauracji: listę składników, posiłków, zestawów
 * @author Inga Spychała
 */
public class Menu implements Serializable{
    private ArrayList<Posilek> listaPosilkow = new ArrayList(); 
    private ArrayList<Zestaw> listaZestawow = new ArrayList();
    private String[] skladniki = new String[25];

    public Menu(){
        for(int i = 0; i<skladniki.length; i++){
            String skl = randomAlphabetic(3,5);
            skladniki[i] = skl;
        }
        for(int i = 0; i<20; i++){
            Posilek p = new Posilek(this);
            listaPosilkow.add(p);
        }
        for(int i = 0; i<10; i++){
            Zestaw z = new Zestaw(this);
            listaZestawow.add(z);
        }
    }
    
    /**
     * 
     * @param p nowy posiłek
     */
    public void dodajPosilek(Posilek p){
        this.listaPosilkow.add(p);
    }
    
    /**
     * 
     * @param z nowy zestaw
     */
    public void dodajZestaw(Zestaw z){
        this.listaZestawow.add(z);
    }

    /**
     * @return lista posiłków
     */
    public ArrayList<Posilek> getListaPosilkow() {
        return listaPosilkow;
    }

    /**
     * @return lista zestawów
     */
    public ArrayList<Zestaw> getListaZestawow() {
        return listaZestawow;
    }

    @Override
    public String toString() { 
        return "Menu; posiłki: " + listaPosilkow.size() + ", zestawy: " + listaZestawow.size();
    }

    /**
     * @return lista skladników
     */
    public String[] getSkladniki() {
        return skladniki;
    }
}
