package projekt2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Klasa przechowująca dane restauracji w postaci list: klientów, dostawców,
 * zamówień
 *
 * @author Inga Spychała
 */
public class Dane implements Serializable {

    private static final long serialVersionUID = 1L;
    private List<Dostawca> listaDostawcow;
    private List<KlientOkazjonalny> listaKlientowOkazjonalnych;
    private List<KlientStaly> listaKlientowStalych;
    private List<KlientFirmowy> listaKlientowFirmowych;
//    private ArrayList<Pojazd> listaPojazdow;
    private List<Zamowienie> listaZamowien;
    private List<Zamowienie> gotoweZamowienia;
    private Menu m;
    private int[][] lokalizacja = new int[5][8];

    public Dane() {
        
        listaDostawcow = Collections.synchronizedList(new ArrayList<>());
        listaKlientowOkazjonalnych = Collections.synchronizedList(new ArrayList<>());
        listaKlientowStalych = Collections.synchronizedList(new ArrayList<>());
        listaKlientowFirmowych = Collections.synchronizedList(new ArrayList<>());
//        this.listaPojazdow = new ArrayList<>();
        listaZamowien = Collections.synchronizedList(new ArrayList<>());
        gotoweZamowienia = Collections.synchronizedList(new ArrayList<>());
        for (int i = 0; i < 5; i++) {
            Arrays.fill(this.lokalizacja[i], 0);
        }
        this.lokalizacja[2][4] = 5;
        this.m = new Menu();
    }

    /**
     * @return listaDostawcow
     */
    public synchronized List<Dostawca> getListaDostawcow() {
        return listaDostawcow;
    }

    /**
     * @param d dostawca
     * @param op dodanie lub usunięcie dostawcy z listy
     */
    public void modifyListaDostawcow(Dostawca d, int op) {
        synchronized (listaDostawcow) {
            if (op == 1) {
                this.listaDostawcow.add(d);
            } else if (op == -1) {
                this.listaDostawcow.remove(d);
            }
        }
    }

    /**
     * @return listaKlientowOkazjonalnych
     */
    public synchronized List<KlientOkazjonalny> getListaKlientowOkazjonalnych() {
        return listaKlientowOkazjonalnych;
    }

    /**
     * @param ko klient okazjonalny
     * @param op dodanie lub usunięcie klienta okazjonalnego z listy
     */
    public void modifyListaKlientowOkazjonalnych(KlientOkazjonalny ko, int op) {
        synchronized (listaKlientowOkazjonalnych) {
            if (op == 1) {
                this.listaKlientowOkazjonalnych.add(ko);
            } else if (op == -1) {
                this.listaKlientowOkazjonalnych.remove(ko);
            }
        }

    }

    /**
     * @return listaKlientowStalych
     */
    public synchronized List<KlientStaly> getListaKlientowStalych() {
        return listaKlientowStalych;
    }

    /**
     * @param ks klient stały
     * @param op dodanie lub usunięcie klienta stałego z listy
     */
    public void modifyListaKlientowStalych(KlientStaly ks, int op) {
        synchronized (listaKlientowStalych) {
            if (op == 1) {
                this.listaKlientowStalych.add(ks);
            } else if (op == -1) {
                this.listaKlientowStalych.remove(ks);
            }
        }

    }

    /**
     * @return listaKlientowFirmowych
     */
    public synchronized List<KlientFirmowy> getListaKlientowFirmowych() {
        return listaKlientowFirmowych;
    }

    /**
     * @param kf klient firmowy
     * @param op dodanie lub usunięcie klienta firmowego z listy
     */
    public void modifyListaKlientowFirmowych(KlientFirmowy kf, int op) {
        synchronized (listaKlientowFirmowych) {
            if (op == 1) {
                this.listaKlientowFirmowych.add(kf);
            } else if (op == -1) {
                this.listaKlientowFirmowych.remove(kf);
            }
        }

    }

//    /**
//     * @return listaPojazdow
//     */
//    public ArrayList<Pojazd> getListaPojazdow() {
//        return listaPojazdow;
//    }
//
//    /**
//     * @param p
//     * @param op
//     */
//    public synchronized void modifyListaPojazdow(Pojazd p, int op) {
//        if(op == 1) {
//            this.listaPojazdow.add(p);
//        } else if (op == -1) {
//            this.listaPojazdow.remove(p);
//        }
//    }
    /**
     * @return listaZamowien
     */
    public synchronized List<Zamowienie> getListaZamowien() {
        return listaZamowien;
    }

    /**
     * @param z zamówienie
     * @param op dodanie lub usunięcie zamówienia z listy zamówień
     */
    public void modifyListaZamowien(Zamowienie z, int op) {
        synchronized (listaZamowien) {
            if (op == 1) {
                this.listaZamowien.add(z);
            } else if (op == -1) {
                this.listaZamowien.remove(z);
            }
        }

    }

    /**
     * @return listaZamowien
     */
    public synchronized List<Zamowienie> getGotoweZamowienia() {
        return listaZamowien;
    }

    /**
     * @param z gotowe zamówienie
     * @param op dodanie lub usunięcie zamówienia z listy gotowych zamówień
     */
    public void modifyGotoweZamowienia(Zamowienie z, int op) {
        synchronized (gotoweZamowienia) {
            if (op == 1) {
                this.gotoweZamowienia.add(z);
            } else if (op == -1) {
                this.gotoweZamowienia.remove(z);
            }
        }

    }

    /**
     * @return menu
     */
    public Menu getM() {
        return m;
    }

//    /**
//     * @param m menu
//     */
//    public synchronized void setM(Menu m) {
//        this.m = m;
//    }
    /**
     * @param x numer domu w rzędzie
     * @param y numer rzędu, w którym znajduje się dom
     * @return wartość domu: 0-brak klienta, 1-klient okazjonalny, 2-klient
     * stały, 3-klient firmowy, 5-restauracja
     */
    public synchronized int getLokalizacja(int x, int y) {
        return lokalizacja[y][x];
    }

    /**
     * @param x numer domu w rzędzie
     * @param y numer rzędu, w którym znajduje się dom
     * @param val wartość domu: 0-brak klienta, 1-klient okazjonalny, 2-klient
     * stały, 3-klient firmowy
     */
    public synchronized void setLokalizacja(int x, int y, int val) {
        this.lokalizacja[y][x] = val;
    }

}
