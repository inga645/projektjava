/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.io.Serializable;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 * Klasa opisująca posiłki. 
 * Każdy posiłek ma przypisaną nzawę, kategorię, rozmiar, listę składników, cenę, czas przygotowania
 * @author Inga Spychała
 */
public class Posilek implements Serializable{
    private final String[] kategorie = {"przystawka","zupa","danieglowne","dodatki","deser","napoj"};
    String nazwa;
    private String[] skladniki;
    double cena;
    private String kategoria;
    private int rozmiar;
    int czasPrzygotowania;

    public Posilek(Menu m){
        this.nazwa = randomAlphabetic(10);
        this.rozmiar = ThreadLocalRandom.current().nextInt(3);
        int c = ThreadLocalRandom.current().nextInt(10,25);
        if(this.rozmiar == 0){
            this.cena = c-1;
        } else if (this.rozmiar == 1){
            this.cena = c;
        } else {
            this. cena = c+1;
        }
        
        this.czasPrzygotowania = ThreadLocalRandom.current().nextInt(1,15);
        
        int k = ThreadLocalRandom.current().nextInt(6);
        this.kategoria = kategorie[k];
        
        int lskl = ThreadLocalRandom.current().nextInt(1,5);
        this.skladniki = new String[lskl];
        for(int i = 0; i < lskl; i++){
            int tmp = ThreadLocalRandom.current().nextInt(m.getSkladniki().length);
            this.skladniki[i] = m.getSkladniki()[tmp];
        }
        
    }
    
    /**
     * @return nazwa posiłku
     */
    public String getNazwa() {
        return nazwa;
    }

    /**
     * @return skladniki posiłku
     */
    public String[] getSkladniki() {
        return skladniki;
    }

    /**
     * @return cena posiłku
     */
    public double getCena() {
        return cena;
    }

    /**
     * @return czas przygotowania
     */
    public int getCzasPrzygotowania() {
        return czasPrzygotowania;
    }
    
    /**
     * Przygotowanie posiłku trwa przez określony czas
     * @throws InterruptedException 
     */
    public void przygotuj() throws InterruptedException{
        Thread.sleep(1000*this.czasPrzygotowania);
    }
    
    @Override
    public String toString(){
        return "posiłek: nazwa: " + nazwa + ", kategoria: " + kategoria 
                + ", składniki: " + Arrays.toString(skladniki)
                + ", rozmiar: " + rozmiar + ", cena: " + cena
                + ", czas przygotowania: " + czasPrzygotowania;
    }
}
