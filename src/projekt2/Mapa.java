/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

/**
 *
 * @author Inga Spychała 
 * Klasa zajmująca się tworzeniem mapy i zaznaczaniem na
 * niej dostawców oraz klientów.
 *
 */
public class Mapa extends JPanel {

    Image tlo;
    Image klientOkazjonalnyImage;
    Image klientFirmowyImage;
    Image klientStalyImage;
    Image dostawca;
    Dane info;
    
    List<Dostawca> dost;
    
    private int x;
    private int y;

    public Mapa(Dane d) throws IOException {
        tlo = ImageIO.read(this.getClass().getResource("mapa2.png"));
        klientOkazjonalnyImage = ImageIO.read(this.getClass().getResource("klientokazjonalny.png"));
        klientStalyImage = ImageIO.read(this.getClass().getResource("klientstaly.png"));
        klientFirmowyImage = ImageIO.read(this.getClass().getResource("klientfirma.png"));
        dostawca = ImageIO.read(this.getClass().getResource("delivery.png"));
        this.info = d;
    }
    

    @Override
    public void paintComponent(Graphics g) {
        g.clearRect(0, 0, this.getWidth(), this.getHeight());
        g.drawImage(tlo, 0, 0, null);
               
        for (int i = 0; i < info.getListaKlientowStalych().size(); i++) {
            x = info.getListaKlientowStalych().get(i).getWspolrzedne().x;
            y = info.getListaKlientowStalych().get(i).getWspolrzedne().y;
            g.drawImage(klientStalyImage, x, y, null);
        }
        for (int i = 0; i < info.getListaKlientowOkazjonalnych().size(); i++) {
            x = info.getListaKlientowOkazjonalnych().get(i).getWspolrzedne().x;
            y = info.getListaKlientowOkazjonalnych().get(i).getWspolrzedne().y;
            g.drawImage(klientOkazjonalnyImage, x, y, null);
        }
        for (int i = 0; i < info.getListaKlientowFirmowych().size(); i++) {
            x = info.getListaKlientowFirmowych().get(i).getWspolrzedne().x;
            y = info.getListaKlientowFirmowych().get(i).getWspolrzedne().y;
            g.drawImage(klientFirmowyImage, x, y, null);
        }
        for (int i = 0; i < info.getListaDostawcow().size(); i++) {
            x = (int) info.getListaDostawcow().get(i).getWspolrzedne().getX();
            y = (int) info.getListaDostawcow().get(i).getWspolrzedne().getY();
            g.drawImage(dostawca, x, y, null);
        }
    }
}
