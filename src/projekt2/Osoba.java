/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.awt.Point;
import java.io.Serializable;

/**
 *
 * @author Inga Spychała
 * Klasa opisująca wszystkie osoby związane z restauracją.
 */
public class Osoba implements Runnable, Serializable{
    Point wspolrzedne;

    /**
     * @return współrzędne osoby na mapie
     */
    public Point getWspolrzedne() {
        return wspolrzedne;
    }

    /**
     * @param wspolrzedne wspolrzedne osoby na mapie
     */
    public void setWspolrzedne(Point wspolrzedne) {
        this.wspolrzedne = wspolrzedne;
    }
    
    @Override
    public void run(){}
}
