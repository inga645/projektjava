/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.awt.Point;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;

/**
 *
 * @author Inga Spychała
 * 
 * Klasa opisująca pracowników restauracji.
 */
public class Dostawca extends Osoba{

    private int idDost;
    private String imie;
    private String nazwisko;
    private final int[] pesel = new int[11];
    private String[] zmiany = new String[7];
    private int[] prawoJazdy = {0,0};
    private Dane info;
    private Pojazd lok;
    private boolean awaria;
    
    
    public Dostawca(Dane d, int id){
        this.awaria = false;
        this.idDost = id;
        this.wspolrzedne = new Point(345,190);
        this.imie = randomAlphabetic(10);
        this.nazwisko = randomAlphabetic(15);
        
        for(int i =0 ; i<7; i++){
            String s = "";
            int x = ThreadLocalRandom.current().nextInt(24);
            int y = ThreadLocalRandom.current().nextInt(60);
            int z = ThreadLocalRandom.current().nextInt(x,24);
            int u = ThreadLocalRandom.current().nextInt(60);
            s = x + ":" + y + " - " + z + ":" + u;
            zmiany[i] = s;            
        }
        
        for(int i = 0; i<11; i++){
            this.pesel[i] = ThreadLocalRandom.current().nextInt(10);
        }
        
        int lpj = ThreadLocalRandom.current().nextInt(3);
        if(lpj == 2){
            this.prawoJazdy[0] = 1; //kategoria A
            this.prawoJazdy[1] = 1; //kategoria B
        } else if (lpj == 1) {
            this.prawoJazdy[1] = 1;
        } else {
            this.prawoJazdy[0] = 1;
        }
        
        this.info = d;
    }
    
    /**
     * w pętli nieskończonej dostawca wybiera pojazd, sprawdza i rozwozi zamówienia, tankuje pojazd
     */
    @Override
    public void run(){
        while(true){
            wybierzPojazd();
            sprawdzZamowienia();
            if(!lok.getBagaz().isEmpty()){
                rozwiezZamowienia();
            }
            getLok().zatankuj();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Dostawca.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    /**
     * 
     * @param pt punkt docelowy dostawcy 
     */
    public void jedz(Point pt){
        if(pt.x < this.wspolrzedne.x){
            while(pt.x+12 != this.wspolrzedne.x){
                this.wspolrzedne.x--;
                lok.setStanPaliwa(0.0125);
                try {
                    Thread.sleep((long) (100-lok.getPredkosc()));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Dostawca.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(awaria){
                    awaryjnyPowrot();
                    break;
                }
            }
        } else if(pt.x > this.wspolrzedne.x) {
            while(pt.x+12 != this.wspolrzedne.x){
                this.wspolrzedne.x++;
                lok.setStanPaliwa(0.0125);
                try {
                    Thread.sleep((long) (100-lok.getPredkosc()));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Dostawca.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(awaria){
                    awaryjnyPowrot();
                    break;
                }
            }
        }
        if(pt.y < this.wspolrzedne.y){
            while(pt.y+12 != this.wspolrzedne.y){
                this.wspolrzedne.y--;
                lok.setStanPaliwa(0.0125);
                try {
                    Thread.sleep((long) (100-lok.getPredkosc()));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Dostawca.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(awaria){
                    awaryjnyPowrot();
                    break;
                }
            }
        } else if(pt.y > this.wspolrzedne.y) {
            while(pt.y+12 != this.wspolrzedne.y){
                this.wspolrzedne.y++;
                lok.setStanPaliwa(0.0125);
                try {
                    Thread.sleep((long) (100-lok.getPredkosc()));
                } catch (InterruptedException ex) {
                    Logger.getLogger(Dostawca.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(awaria){
                    awaryjnyPowrot();
                    break;
                }
            }
        }
    }
    
    /**
     * dostawca wybiera pojazd na podstawie swoich uprawnień
     * w zależności od posiadanych kategorii prawa jazdy może wybrać skuter i/lub samochód
     */
    public void wybierzPojazd(){
        if(this.prawoJazdy[0]==1 && this.prawoJazdy[1]==1){
            int x = ThreadLocalRandom.current().nextInt(2);
            if(x==0){
                lok = new Skuter();
            } else {
                lok = new Samochod();
            }
        } else if (this.prawoJazdy[0]==1 && this.prawoJazdy[1]==0){
            lok = new Skuter();
        } else if(this.prawoJazdy[0]==0 && this.prawoJazdy[1]==1){
            lok = new Samochod();
        }
    }
    
    /**
     * Dostawca sprawdza, czy są gotowe zamówienia, które nie zostały rozwiezione i pakuje je do pojazdu
     */
    public void sprawdzZamowienia(){
        getLok().zaladuj(info);
    }
    
    /**
     * Dostawca rozwozi zamowienia, zapakowane do używanego pojazdu
     */
    public void rozwiezZamowienia(){
        while(!lok.getBagaz().empty()){
            Zamowienie z = getLok().getBagaz().peek();
            jedz(z.getLokalizacja());
            this.lok.deleteFromBagaz();
        }
        jedz(new Point(328,178));
    }
   
    
    /**
     * Zmusza dostawcę do natychmiastowego powrotu do restauracji, nawet jeśli nie rozwiózł wszystkich zamówień
     * Po powrocie opróżnia pojazd, odkładając niedostarczone zamówienia na listę zamówień gotowych
     */
    public void awaryjnyPowrot(){
        jedz(new Point(328,178));
        while(!this.lok.getBagaz().isEmpty()){
            Zamowienie z = this.lok.deleteFromBagaz();
            info.modifyGotoweZamowienia(z, 1);
        }
        awaria = false;
    }
    
    
    @Override
    public String toString(){
        return "Dostawca: " + imie + " " + nazwisko + " " + Arrays.toString(pesel)
                + ", prawo jazdy: " + Arrays.toString(prawoJazdy)
                + ", zmiany: " + Arrays.toString(zmiany);
    }

    /**
     * @return numer ID dostawcy
     */
    public int getIdDost() {
        return idDost;
    }
    
    
        
    /**
     * @return imie dostawcy
     */
    public String getImie() {
        return imie;
    }

    /**
     * @return nazwisko dostawcy
     */
    public String getNazwisko() {
        return nazwisko;
    }

    /**
     * @return numer pesel dostawcy
     */
    public int[] getPesel() {
        return pesel;
    }

    /**
     * @return lista kategorii prawa jazdy dostawcy (A i/lub B)
     */
    public int[] getPrawoJazdy() {
        return prawoJazdy;
    }
    
    /**
     * @return pojazd używany przez dostawcę
     */
    public Pojazd getLok() {
        return lok;
    }

    /**
     * @param awaria czy dostawca musi awaryjnie wrócić do restauracji
     */
    public void setAwaria(boolean awaria) {
        this.awaria = awaria;
    }

}
