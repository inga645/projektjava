/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Stack;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Klasa opisująca pojazdy, których używają dostawcy
 * @author Inga Spychała
 */
public class Pojazd implements Serializable{

    protected int ladownosc;
    protected float predkosc;
    private char[] numerRejestracyjny = new char[7];
    protected double pojemnoscBaku;
    protected double stanPaliwa;
    private Stack<Zamowienie> bagaz = new Stack();
    
    public Pojazd(){
        String x = RandomStringUtils.randomAlphabetic(3);
        for(int i = 0; i < 3; i++){
            this.numerRejestracyjny[i] = x.charAt(i);
        }
        for(int i = 3; i < 7; i++){
            this.numerRejestracyjny[i] = (char) ThreadLocalRandom.current().nextInt(10);
        }
    }
    
    /**
     * Pakuje gotowe zamówienia do pojazdu
     * @param d dane restauracji
     */
    public void zaladuj(Dane d){
        while(!d.getGotoweZamowienia().isEmpty() && this.bagaz.size() < this.ladownosc){
            Zamowienie z = d.getGotoweZamowienia().get(0);
            d.getGotoweZamowienia().remove(z);
            //d.modifyGotoweZamowienia(z, -1);
            this.getBagaz().push(z);
            
        }
    }
    
    /**
     * Pojazd jest tankowany do pełna po każdym powrocie do restauracji
     */
    public void zatankuj(){
        this.stanPaliwa = this.pojemnoscBaku;
    }

    /**
     * @return maksymalną liczbę zamówień dla pojazdu
     */
    public int getLadownosc() {
        return ladownosc;
    }

    /**
     * @return predkość pojazdu
     */
    public float getPredkosc() {
        return predkosc;
    }

    /**
     * @return pojemność baku pojazdu
     */
    public double getPojemnoscBaku() {
        return pojemnoscBaku;
    }

    /**
     * @return stan paliwa
     */
    public double getStanPaliwa() {
        return 100*(this.stanPaliwa/this.pojemnoscBaku);
    }

    /**
     * @param stanPaliwa ilość wykorzystanego paliwa
     */
    public void setStanPaliwa(double stanPaliwa) {
        this.stanPaliwa -= stanPaliwa;
    }
    
    
    /**
     * @return zawartość bagażnika
     */
    public Stack<Zamowienie> getBagaz() {
        return bagaz;
    }

    /**
     * @return zamówienie na górze stosu, którym jest bagaż
     */
    public Zamowienie deleteFromBagaz() {
        return this.bagaz.pop();
    }
    
    @Override
    public String toString() {
        return "ładowność: " + ladownosc + ", prędkość: " + predkosc 
                + ", numer: " + Arrays.toString(numerRejestracyjny) 
                + ", bak: " + pojemnoscBaku + ", stan: " + stanPaliwa
                + ", ilość zamówień: " + getBagaz().size();
    }
}
