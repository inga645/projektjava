/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;


public class KlientOkazjonalny extends Klient {

    public KlientOkazjonalny(Dane d, int nr) {
        super(d, nr);
        d.setLokalizacja((this.wspolrzedne.x-10)/80, (this.wspolrzedne.y-10)/80, 1);

    }
    
    @Override
    public String toString(){
        return "Okazjonalny " + super.toString();
    }
}
