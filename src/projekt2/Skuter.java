/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projekt2;

/**
 *
 * @author Inga Spychała
 */
public class Skuter extends Pojazd{
    public Skuter(){
        super();
        this.ladownosc = 1;
        this.predkosc = 50;
        this.pojemnoscBaku = 100;
        this.stanPaliwa = this.pojemnoscBaku;
    }
    
    @Override
    public String toString(){
        return "Skuter; " + super.toString();
    }
}
